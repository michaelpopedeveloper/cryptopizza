import * as express from 'express';


export default class MiddlewareController {

  public static CheckIfSessionActive(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (req.isAuthenticated()) return next();
    return res.status(401).send({ msg: 'User session is not active' });
  }

  public static LogRequestUser(req: express.Request, res: express.Response, next: express.NextFunction) {
    console.log('req.user', req.user);
    next();
  }
}
