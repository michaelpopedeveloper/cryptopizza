import CoinbaseAPIController from '../CoinbaseAPIController';
import * as coinbase from 'coinbase-commerce-node';

describe('Test', () => {

  const coinbaseController = new CoinbaseAPIController();

  test('Should set checkout data', (done) => {
    const testCheckoutData: coinbase.CreateCheckout = {
      name: 'test',
      description: 'test description',
      pricing_type: 'fixed_price',
      local_price: {
        amount: '0.01',
        currency: 'USD',
      },
      requested_info: ['email', 'email']
    };
    coinbaseController.setCheckoutData(testCheckoutData);
    expect(coinbaseController.getCheckoutData()).toEqual(testCheckoutData);
    done();
  });


});
