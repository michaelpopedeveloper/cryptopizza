import * as coinbase from 'coinbase-commerce-node';
import * as express from 'express';

export default class CoinbaseAPIController {

  private coinbaseAPIKey: string =
    process.env.coinbaseAPIKey;

  private checkoutData: coinbase.CreateCheckout = null;



  constructor() {
    this.init();
  }

  public setCheckoutData = (checkoutData: coinbase.CreateCheckout) => {
    this.checkoutData = checkoutData;
  }

  public getCheckoutData = () => {
    return this.checkoutData;
  }

  public init = () => {
    coinbase.Client.init(this.coinbaseAPIKey);
    this.setCheckoutData({
      name: 'Did it work?',
      description: 'test description',
      pricing_type: 'fixed_price',
      local_price: {
        amount: '0.01',
        currency: 'USD',
      },
      requested_info: ['email', 'email']
    });
  }

  public createCheckout = (req: any, res: any) => {
    const { Checkout } = coinbase.resources;
    Checkout.create(this.getCheckoutData(), (error, response) => {
      if (error) res.status(400).send({ error });
      res.send({ response });
    });
  }
}
