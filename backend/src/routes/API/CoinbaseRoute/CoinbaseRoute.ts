import * as express from 'express';
import UserController from '../../../controllers/User/UserController';
import CoinbaseAPIController from '../../../controllers/api/coinbase/CoinbaseAPIController';
import MiddlewareController from '../../../controllers/Middleware/MiddlewareController';
import { UserRoute } from '../../User/UserRoute';

const CoinbaseController = new CoinbaseAPIController();

export const CoinbaseRoute: express.Router = express.Router();

CoinbaseRoute.get('/', (req, res) => {
    res.send('coinbase route');
})

CoinbaseRoute.get('/checkout', CoinbaseController.createCheckout);
