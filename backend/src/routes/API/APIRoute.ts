import * as express from 'express';
import UserController from '../../controllers/User/UserController';
import MiddlewareController from '../../controllers/Middleware/MiddlewareController';
import { CoinbaseRoute } from './CoinbaseRoute/CoinbaseRoute';

const passport = require('../../config/passport');

export const APIRoute: express.Router = express.Router();

APIRoute.use(
  MiddlewareController.CheckIfSessionActive,
  MiddlewareController.LogRequestUser);

APIRoute.get('/', (req: express.Request, res: express.Response) => {
  console.log('Go beyond API!');
  res.send('API Home!');
});
APIRoute.use('/commerce', CoinbaseRoute);
