import * as express from 'express';
const router = express.Router();

export const MainRoute = router
.use((req, res, next) => {
  console.log('authentication', req.isAuthenticated());
  next(); 
})
  .get('/', (req, res) => {
    res.send('Home page!');
  });
