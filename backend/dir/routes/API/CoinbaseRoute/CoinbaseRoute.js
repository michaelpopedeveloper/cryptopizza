"use strict";
exports.__esModule = true;
var express = require("express");
var CoinbaseAPIController_1 = require("../../../controllers/api/coinbase/CoinbaseAPIController");
var CoinbaseController = new CoinbaseAPIController_1["default"]();
exports.CoinbaseRoute = express.Router();
exports.CoinbaseRoute.get('/', function (req, res) {
    res.send('coinbase route');
});
exports.CoinbaseRoute.get('/checkout', CoinbaseController.createCheckout);
//# sourceMappingURL=CoinbaseRoute.js.map