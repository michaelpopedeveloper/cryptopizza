"use strict";
exports.__esModule = true;
var express = require("express");
var MiddlewareController_1 = require("../../controllers/Middleware/MiddlewareController");
var CoinbaseRoute_1 = require("./CoinbaseRoute/CoinbaseRoute");
var passport = require('../../config/passport');
exports.APIRoute = express.Router();
exports.APIRoute.use(MiddlewareController_1["default"].CheckIfSessionActive, MiddlewareController_1["default"].LogRequestUser);
exports.APIRoute.get('/', function (req, res) {
    console.log('Go beyond API!');
    res.send('API Home!');
});
exports.APIRoute.use('/commerce', CoinbaseRoute_1.CoinbaseRoute);
//# sourceMappingURL=APIRoute.js.map