"use strict";
exports.__esModule = true;
var CoinbaseAPIController_1 = require("../CoinbaseAPIController");
describe('Test', function () {
    var coinbaseController = new CoinbaseAPIController_1["default"]();
    test('Should set checkout data', function (done) {
        var testCheckoutData = {
            name: 'test',
            description: 'test description',
            pricing_type: 'fixed_price',
            local_price: {
                amount: '0.01',
                currency: 'USD'
            },
            requested_info: ['email', 'email']
        };
        coinbaseController.setCheckoutData(testCheckoutData);
        expect(coinbaseController.getCheckoutData()).toEqual(testCheckoutData);
        done();
    });
});
//# sourceMappingURL=CoinbaseAPIController.test.js.map