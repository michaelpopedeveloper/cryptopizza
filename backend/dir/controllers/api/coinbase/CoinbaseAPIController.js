"use strict";
exports.__esModule = true;
var coinbase = require("coinbase-commerce-node");
var CoinbaseAPIController = (function () {
    function CoinbaseAPIController() {
        var _this = this;
        this.coinbaseAPIKey = process.env.coinbaseAPIKey;
        this.checkoutData = null;
        this.setCheckoutData = function (checkoutData) {
            _this.checkoutData = checkoutData;
        };
        this.getCheckoutData = function () {
            return _this.checkoutData;
        };
        this.init = function () {
            coinbase.Client.init(_this.coinbaseAPIKey);
            _this.setCheckoutData({
                name: 'Did it work?',
                description: 'test description',
                pricing_type: 'fixed_price',
                local_price: {
                    amount: '0.01',
                    currency: 'USD'
                },
                requested_info: ['email', 'email']
            });
        };
        this.createCheckout = function (req, res) {
            var Checkout = coinbase.resources.Checkout;
            Checkout.create(_this.getCheckoutData(), function (error, response) {
                if (error)
                    res.status(400).send({ error: error });
                res.send({ response: response });
            });
        };
        this.init();
    }
    return CoinbaseAPIController;
}());
exports["default"] = CoinbaseAPIController;
//# sourceMappingURL=CoinbaseAPIController.js.map