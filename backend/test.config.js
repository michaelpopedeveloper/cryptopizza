module.exports = {
    "preset": 'ts-jest',
    "roots": [
        "./src"
    ],
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
}