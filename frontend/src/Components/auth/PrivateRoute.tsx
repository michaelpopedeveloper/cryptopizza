import React, { useState, useEffect } from "react";
import {
  Route,
  Redirect,
} from "react-router-dom";
import axios from 'axios';
import { connect } from 'react-redux';
import cookie from 'react-cookies';
import { loginUser } from '../../actions/index';

const mapStateToProps = state => {
  return { state };
};

function mapDispatchToProps(dispatch) {
  return {
    login: action => dispatch(loginUser(action))
  };
}



interface PrivateRouteProps {
  path: any,
  state: { user: any },
  component: React.Component | any,
  user: any,
  login: (action: any) => any,

}

const PrivateRouteStateless = ({ component: Component, user, ...rest }) => {

//  const [auth, setAuth] = useState(false);

  useEffect(() => {
    localStorage.setItem('connect.sid', cookie.load('connect.sid'))
  })


  return (
    <Route
      {...rest}
      render={(props) =>
        localStorage.getItem('connect.sid') ? (
          <Component {...rest} />
        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
      }
    />
  );
}


export default connect(mapStateToProps, mapDispatchToProps)(PrivateRouteStateless);